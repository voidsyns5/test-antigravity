<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
	public function __construct()
	{
		//
	}

	public function register(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|string',
			'email' => 'required|email|unique:users',
			'password' => 'required',
			'phone' => 'required|integer',
			'address' => 'required'
		]);

		$input = $request->only('name', 'email', 'password', 'phone', 'address');

		try {
			$model = new User;
			$model->name = $input['name'];
			$model->email = $input['email'];
			$model->password = Hash::make($input['password']);
			$model->phone = $input['phone'];
			$model->address = $input['address'];

			if ($model->save()) {
				$status = 200;
				$output = [
					'user' => $model,
					'status' => $status,
					'message' => 'User has been registered successfully'
				];
			} else {
				$status = 500;
				$output = [
					'status' => $status,
					'message' => 'An error occured, please refresh the website'
				];
			}
		} catch (Execption $e) {
			$status = $status;
			$output = [
				'code' => 500,
				'message' => 'An error occured, please refresh the website'
			];
		}

		return response()->json($output, $status);
	}

	public function login(Request $request)
	{
		// dd($request);
		$this->validate($request, [
			'email' => 'required|email',
			'password' => 'required'
		]);

		$input = $request->only('email', 'password');
		$authCheck = Auth::attempt($input);

		if (!$authCheck) {
			$status = 401;
			$output = [
				'status' => $status,
				'message' => 'User is Unautherized'
			];
		} else {
			$status = 201;
			$token = $this->respondWithToken($authCheck);
			$output = [
				'status' => $status,
				'token' => $token
			];
		}

		return response()->json($output, $status);
	}

	public function getMe()
	{
		return response()->json(auth()->user());
	}

}